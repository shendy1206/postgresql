1.
select sc1.sid, sc1.cid, sc1.score, sc2.cid, sc2.score
from (select * from sc where cid = '01') sc1
left join (select * from sc where cid = '02') sc2
on sc1.sid = sc2.sid
where sc1.score > sc2.score;

1.1
select sc1.sid, sc1.cid, sc1.score, sc2.sid, sc2.cid, sc2.score
from (select * from sc where cid = '01') sc1
inner join (select * from sc where cid = '02') sc2
on sc1.sid = sc2.sid;

1.2
select sc1.sid, sc1.cid, sc1.score, sc2.sid, sc2.cid, sc2.score
from (select * from sc where cid = '01') sc1
left join (select * from sc where cid = '02') sc2
on sc1.sid = sc2.sid
where sc2.sid is null;

1.3
select sc1.sid, sc1.cid, sc1.score, sc2.sid, sc2.cid, sc2.score
from (select * from sc where cid = '01') sc1
right join (select * from sc where cid = '02') sc2
on sc1.sid = sc2.sid

2.
select 
s.sid, s.sname, c.average
from (
select sid, avg(score) as average
from sc
group by sid
having avg(score) >= 60
)c, student s
where c.sid = s.sid

3.
select 
s.sid, s.sname, s.sage, s.ssex
from (
select sid
from sc
group by sid
)c
left join student s
on c.sid = s.sid

4.
select 
s.sid, s.sname, c.cs, c.scores
from student s
left join (
select sid, count(cid) as cs, sum(score) as scores
from sc
group by sid
) c
on c.sid = s.sid

4.1
select 
s.sid, s.sname, c.cs, c.scores
from student s
left join (
select sid, count(cid) as cs, sum(score) as scores
from sc
group by sid
) c
on c.sid = s.sid
where c.sid is not null

5.
select count(*) 
from teacher
where tname like '李%'

6.
select s.sid, s.sname, s.sage, s.ssex
from student s,
(select sid 
from sc
where cid in 
(select c.cid
from teacher t, course c
where t.tname= '张三'
and t.tid = c.tid)
group by sid) s1
where s.sid = s1.sid


select student.* from student,teacher,course,sc
where 
    student.sid = sc.sid 
    and course.cid=sc.cid 
    and course.tid = teacher.tid 
    and tname = '张三';

7.
select s.sid, s.sname
from student s
where s.sid not in
(
	select sid from sc
	group by sid
	having count(cid) = (select count(cid) from course)
) 

8.
select s.sid, s.sname
from student s
where s.sid in
(
	select distinct sid from sc where cid in (
		select cid from sc where sid = '01'
	)
) 

9.　★　
select * from student where sid in 
(select sid from sc where sid not in
(select sid from sc where cid not in (select cid from sc where sid='01'))
group by sid 
having count(*)=(select count(*) from sc where sid='01') and sid != '01');

10.
select s.sid, s.sname
from student s
where sid not in
(select distinct sid from sc where 
cid in (
select c.cid 
from course c, teacher t
where c.tid = t.tid
and t.tname = '张三')
)

11.
select s.sid, s.sname, c.average
from student s,
(
select sid, avg(score) as average
from sc
where score < 60
group by sid
having count(cid) >= 2) c
where s.sid = c.sid

select student.sid, student.sname, AVG(sc.score) from student,sc
where 
    student.sid = sc.sid and sc.score<60
group by sc.sid 
having count(*)>1;

12.
select s.sid, s.sname, s.sage, s.ssex, c.score
from student s, 
(select sid, score from sc where cid = '01' and score < 60) c
where s.sid = c.sid
order by c.score desc


select student.*, sc.score from student, sc
where student.sid = sc.sid
and sc.score < 60
and cid = '01'
ORDER BY sc.score DESC;

13.
select c1.sid, c1.cid, c1.score, c2.average
from sc c1, 
(select sid, avg(score) as average from sc group by sid) c2
where c1.sid = c2.sid
order by c2.average desc, c1.sid, c1.cid

14.
select sc.cid, max(c.cname),  count(sc.sid) as ssize, 
max(sc.score), min(sc.score), avg(sc.score),
count(case when sc.score >= 60 then sc.sid end) * 100 / count(sc.sid) as pass,
count(case when sc.score >= 70 and sc.score < 80 then sc.sid end) * 100 / count(sc.sid) as C,
count(case when sc.score >= 80 and sc.score < 90 then sc.sid end) * 100 / count(sc.sid) as B,
count(case when sc.score >= 90 then sc.sid end)  * 100 / count(sc.sid) as A
from sc, course c
where sc.cid = c.cid
group by sc.cid
order by ssize desc, sc.cid asc


select 
sc.CId ,
max(sc.score)as 最高分,
min(sc.score)as 最低分,
AVG(sc.score)as 平均分,
count(*)as 选修人数,
sum(case when sc.score>=60 then 1 else 0 end ) * 100/count(*)as 及格率,
sum(case when sc.score>=70 and sc.score<80 then 1 else 0 end ) * 100/count(*)as 中等率,
sum(case when sc.score>=80 and sc.score<90 then 1 else 0 end ) * 100/count(*)as 优良率,
sum(case when sc.score>=90 then 1 else 0 end ) * 100/count(*)as 优秀率 
from sc
GROUP BY sc.CId
ORDER BY count(*)DESC, sc.CId ASC

15. ★
select a.cid, a.sid, a.score, count(b.score)+1 as rank
from sc as a 
left join sc as b 
on a.score<b.score and a.cid = b.cid
group by a.cid, a.sid,a.score
order by a.cid, rank ASC;

15.1  ★
select a.* ,count(b.score)+1 rank from sc a left join sc b
	on a.cid = b.cid and (a.score < b.score or (a.score = b.score and a.sid > b.sid))
	group by a.cid,a.sid, a.score
	order by a.cid,count(b.score)

16
select a.sid, count(b.sum) + 1 as rank, a.sum
from
(select sid, sum(score) sum
from sc
group by sid 
order by sum) a left join
(select sid, sum(score) sum
from sc
group by sid
order by sum) b
on a.sum < b.sum
group by a.sid
order by rank


set @crank=0;
select q.sid, total, @crank := @crank +1 as rank from(
select sc.sid, sum(sc.score) as total from sc
group by sc.sid
order by total desc)q;

select k1.*, count(k2.total) rank
from (select SId, sum(score) total
          from sc
          group by SId) k1,
         (select sum(score) total
          from sc
          group by SId) k2
where k2.total>=k1.total
group by k1.SId, k1.total
order by rank

16.1
select k1.*, count(distinct k2.total) rank
from (select SId, sum(score) total
          from sc
          group by SId) k1,
         (select sum(score) total
          from sc
          group by SId) k2
where k2.total>=k1.total
group by k1.SId, k1.total
order by rank

17．
select sc.cid, max(c.cname),
count(case when sc.score > 85 and sc.score <= 100 then sc.sid end) as A,
count(case when sc.score > 85 and sc.score <= 100 then sc.sid end) * 100 / count(*) as AP,
count(case when sc.score > 70 and sc.score <= 85 then sc.sid end) as B,
count(case when sc.score > 70 and sc.score <= 85 then sc.sid end) * 100 / count(*) as BP,
count(case when sc.score > 60 and sc.score <= 70 then sc.sid end) as C,
count(case when sc.score > 60 and sc.score <= 70 then sc.sid end) * 100 / count(*) as CP,
count(case when sc.score > 0 and sc.score <= 60 then sc.sid end) as D,
count(case when sc.score > 0 and sc.score <= 60 then sc.sid end) * 100/ count(*) as DP
from sc, course c
where sc.cid = c.cid
group by sc.cid

#mysql??
select c.CId 课程编号,
           c.Cname 课程名称,
           count(case when sc.score>=85 then 1 else null end) as '[100,85]人数',
           round(count(case when sc.score>=85 then 1 else null end)*1.0/count(sc.score),2) as '[100,85]占比',
           count(case when sc.score>=70 and sc.score<85 then 1 else null end) as '(85,70]人数',
           round(count(case when sc.score>=70 and sc.score<85 then 1 else null end)*1.0/count(sc.score),2) as '(85,70]占比',
           count(case when sc.score>=60 and sc.score<70 then 1 else null end) as '(70,60]人数',
           round(count(case when sc.score>=60 and sc.score<70 then 1 else null end)*1.0/count(sc.score),2) as '(70,60]占比',
           count(case when sc.score<60 then 1 else null end) as '(60,0]人数',
           round(count(case when sc.score<60 then 1 else null end)*1.0/count(sc.score),2) as '(60,0]占比'
from Course c left join sc
on c.CId=sc.CId
group by c.CId


18.
select a.cid, a.sid, count(distinct b.score) + 1 as rank
from sc a
left join sc b
on a.cid = b.cid and a.score < b.score
group by a.cid, a.sid
having count(distinct b.score) < 3
order by a.cid, rank

select a.cid, a.sid, count(b.score) + 1 as rank, a.score
from sc a
left join sc b
on a.cid = b.cid and a.score < b.score
group by a.cid, a.sid
having count(b.score) < 3
order by a.cid, rank

select CId, SId, score
from sc
where (select count(*) from sc sc1 where sc1.CId=sc.CId and sc1.score>sc.score) <3
order by CId, score desc

19.
select cid, count(sid)
from sc
group by cid

20.
select sc.sid, s.sname, count(cid)
from sc, student s
where sc.sid = s.sid
group by sc.sid, s.sname
having count(cid) = 2
order by sc.sid

21.
select ssex, count(sid)
from student
group by ssex

22.
select sid, sname, ssex, sage
from student
where sname like '%风%'

23.
select sname, count(sid)
from student
group by sname
order by sname

24.
select sid, sname,sage
from student
where 
sage between '1990-01-01' and '1990-12-31'
order by sage

25.
select sc.cid, c.cname, avg(sc.score) as average
from sc, course c
where sc.cid = c.cid
group by sc.cid, c.cname
order by average desc, sc.cid

26.
select sc.sid, s.sname, avg(sc.score) as average
from sc, student s
where sc.sid = s.sid
group by sc.sid, s.sname
having avg(sc.score) > 85

27.
select s.sname, sc.score
from student s, sc, course c
where s.sid = sc.sid
and sc.cid = c.cid
and c.cname = '数学'
and sc.score < 60

28.
select s.sid, s.sname, sub.cname, sub.score
from student s
left join (
select sc.sid, c.cname, sc.score
from sc, course c
where sc.cid = c.cid
) sub
on s.sid = sub.sid

29.
#理解間違った
select sc.sid, s.sname, c.cname, sc.score
from sc, student s, course c
where sc.sid not in (select distinct sid from sc where score < 70)
and sc.sid = s.sid
and sc.cid = c.cid

#正しい
select student.Sname,course.Cname,sc.score
from student , sc  ,course
where sc.score>=70
and  student.SId=sc.SId
and sc.CId=course.CId

30.
select sc.sid, s.sname, c.cname, sc.score
from sc, student s, course c
where sc.sid in (select distinct sid from sc where score < 60)
and sc.sid = s.sid
and sc.cid = c.cid

select DISTINCT sc.CId
from sc
where sc.score <60

31.
select sc.sid, s.sname
from sc, student s
where sc.sid = s.sid
and sc.cid = '01'
and sc.score >= 80

32.
select cid,  count(sid)
from sc
group by cid

33.
select sc.sid, s.sname, sc.score, sc.cid
from student s, sc
where 
s.sid = sc.sid
and sc.cid in
(select c.cid from course c, teacher t 
where t.tid = c.tid and t.tname = '张三')
order by sc.score desc
limit 1

34.
UPDATE sc SET score=90
where sid = '07'
and cid = '02';

select sc.sid, s.sname, sc.score, sc.cid
from student s, sc, 
(select c.cid, max(sc.score) as mxScore from course c, teacher t, sc
where t.tid = c.tid and t.tname = '张三'
and sc.cid = c.cid
group by c.cid) c
where 
s.sid = sc.sid
and sc.cid = c.cid
and sc.score = c.mxScore



select student.*, sc.score, sc.cid from student, teacher, course,sc 
where teacher.tid = course.tid
and sc.sid = student.sid
and sc.cid = course.cid
and teacher.tname = '张三'
and sc.score = (
    select Max(sc.score) 
    from sc,student, teacher, course
    where teacher.tid = course.tid
    and sc.sid = student.sid
    and sc.cid = course.cid
    and teacher.tname = '张三'
);

35.
select a.sid, a.cid, a.score
from sc a, sc b
where 
a.cid <> b.cid
and a.score = b.score
and a.sid =  b.sid
group by a.sid, a.cid, a.score
order by a.cid, a.sid

select distinct sc1.*
from sc sc1 inner join sc sc2
on sc1.SId=sc2.SId and sc1.score=sc2.score and sc1.CId!=sc2.CId

36.
UPDATE sc SET score=80
where sid = '05'
and cid = '01';

select a.sid, a.cid, a.score, count(distinct b.score) + 1 as rank
from sc a
left join sc b
on a.score < b.score and a.cid = b.cid
group by a.sid, a.cid, a.score
having count(b.score) < 2
order by a.cid, rank

select a.sid, a.cid, a.score, count(b.score) + 1 as rank
from sc a
left join sc b
on a.score < b.score and a.cid = b.cid
group by a.sid, a.cid, a.score
having count(b.score) < 2
order by a.cid, rank

select *
from sc
where (select count(*)
             from sc sc1
             where sc1.CId=sc.CId and sc1.score>sc.score)<2
order by CId

37.
select cid, count(sid) as num
from sc
group by cid
having count(sid) >= 5

select CId, count(*)
from sc
group by CId
having count(*)>5

38.
select sid
from sc
group by sid
having count(cid) >= 2
order by sid

39.
select sc.sid, max(s.sname), max(s.sage), max(s.ssex)
from sc
left join student s
on s.sid = sc.sid
group by sc.sid
having count(sc.cid) >= (select distinct count(1) from course)
order by sc.sid

40.
select SId as 学生编号,
           Sname  as  学生姓名,
           extract(year from now()) - extract(year from sage) as 学生年龄
from student

41.
select SId as 学生编号,
           Sname  as  学生姓名,
           case when
           extract(month from now()) < extract(month from sage) or
           (extract(month from now()) = extract(month from sage) and extract(day from now()) < extract(day from sage))
           then extract(year from now()) - extract(year from sage) - 1
           else extract(year from now()) - extract(year from sage) end as 学生年龄
from student

select SId as 学生编号,
           Sname  as  学生姓名,
           date_part('year', age(sage)) as 学生年龄
from student

42.
SELECT    date_trunc('week', '2019-12-05 22:24:22'::timestamp)::date
   || ' '
   || (date_trunc('week', '2019-12-05 22:24:22'::timestamp)+ '6 days'::interval)::date;


select cast(date_trunc('week', current_date) as date) + i
from generate_series(0,6) i

select cast(date_trunc('week', current_date) as date) + 0 || '-->' ||  cast(date_trunc('week', current_date) as date) + 6;


#has birthday tommorrow
SELECT * FROM student
WHERE to_char(sage, 'MM-DD') = to_char(now() + '1 day'::INTERVAL, 'MM-DD');

SELECT * FROM student
WHERE to_char(sage, 'MM-DD') >= to_char(cast(date_trunc('week', current_date) as date) + 0, 'MM-DD')
and to_char(sage, 'MM-DD') <= to_char(cast(date_trunc('week', current_date) as date) + 6, 'MM-DD')

update student set sage = '2017-12-08'
where sid = '09';
update student set sage = '2017-12-09'
where sid = '10';
update student set sage = '2012-12-02'
where sid = '11';

#having birthday this week
SELECT * FROM student
WHERE to_char(sage, 'MM-DD') >= to_char(cast(date_trunc('week', current_date) as date) + 0, 'MM-DD')
and to_char(sage, 'MM-DD') <= to_char(cast(date_trunc('week', current_date) as date) + 6, 'MM-DD')

43.

update student set sage = '2017-12-15'
where sid = '12';
update student set sage = '2012-12-16'
where sid = '13';

SELECT * FROM student
WHERE to_char(sage, 'MM-DD') >= to_char(cast(date_trunc('week', current_date) as date) + 7, 'MM-DD')
and to_char(sage, 'MM-DD') <= to_char(cast(date_trunc('week', current_date) as date) + 13, 'MM-DD')

44.
SELECT * FROM student
WHERE extract('month' FROM  sage) = extract('month' FROM now())

45.
SELECT * FROM student
WHERE extract('month' FROM  sage) = extract('month' FROM now() + '1 month'::INTERVAL)




